/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_furmigueiro;

import recursos.interfaces.IPair;

/**
 *
 * @author Lenovo
 */
public class Pair implements IPair {

    private Object primeiro;
    private Object segundo;

    public Pair(Object primeiro, Object segundo) {
        this.primeiro = primeiro;
        this.segundo = segundo;
    }

    
    @Override
    public Object getFirst() {
        return this.primeiro;
    }

    @Override
    public Object getSecond() {
        return this.segundo;
    }

}
