/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_furmigueiro;

import java.util.Iterator;
import recursos.exceptions.EmptyCollectionException;
import recursos.interfaces.IComida;
import recursos.interfaces.ISilo;

/**
 *
 * @author Lenovo
 */
public class Silo extends Sala implements ISilo {
    private int quantidade_comida;
    private int cont;
    private Comida [] comidinha;

    public Silo(int quantidade_comida, int cont, Comida[] comidinha, int id, String descricao, int X, int Y, Formiga[] formignhas) {
        super(id, descricao, X, Y, formignhas, cont);
        this.quantidade_comida = quantidade_comida;
        this.cont = cont;
        this.comidinha = comidinha;
    }
    
    
    @Override
    public void guardaComida(IComida ic) {
       this.comidinha[cont]=(Comida) ic;
        
    }
    private int pesquisa(int temp){
       int pos=-1;
       
       for(int i=0;i<this.cont;i++){
           if(temp==this.comidinha[i].getId()){
               pos=i;
           }
       }
       return pos;
       
    }
    @Override
    public IComida retiraComida() throws EmptyCollectionException {
if(this.cont==0){
    throw new EmptyCollectionException("Vazio");
}
else{
    Comida temp;
   temp=this.comidinha[0];
    for(int i=0;i<this.cont;i++){
        this.comidinha[i]=this.comidinha[i+1];
    }
    return temp;
    
}  
    
    }
    
    @Override
    public Iterator<IComida> iteratorComida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
