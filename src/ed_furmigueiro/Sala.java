/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_furmigueiro;

import recursos.exceptions.ElementNotFoundException;
import recursos.exceptions.EmptyCollectionException;
import recursos.interfaces.IFormiga;
import recursos.interfaces.ISala;
import recursos.interfaces.collections.UnorderedListADT;

/**
 *
 * @author Lenovo
 */
public class Sala implements ISala {

    private int id;
    private String descricao;
    private int X;
    private int Y;
    private Formiga[] formignhas;
    private int cont;

    public Sala(int id, String descricao, int X, int Y, Formiga[] formignhas, int cont) {
        this.id = id;
        this.descricao = descricao;
        this.X = X;
        this.Y = Y;
        this.formignhas = formignhas;
        this.cont = cont;
    }
    
    
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int i) {
        this.id = i;
    }

    @Override
    public int getX() {
        return this.X;
    }

    @Override
    public void setX(int i) {
        this.X = i;
    }

    @Override
    public int getY() {
        return this.Y;
    }

    @Override
    public void setY(int i) {
        this.Y = i;
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public void setDescricao(String string) {
        this.descricao = string;
    }

    @Override
    public void entraFormiga(IFormiga i) {
        this.formignhas[this.cont] = (Formiga) i;
    }

        private int pesquisa(int i) {
        int pos = -1, j = 0;

        while (j < this.cont || pos != -1) {
            if (i == this.formignhas[j].getId()) {
                pos = i;
            }
            j++;
        }

        return pos;
    }
        
    @Override
    public IFormiga saiFormiga(int i) throws EmptyCollectionException, ElementNotFoundException {
        if (this.cont == 0) {
            throw new EmptyCollectionException("Vazio");
        }
        int pos = pesquisa(i);
        if (pos == -1) {
            throw new ElementNotFoundException("Elemento não encontrado");
        } else {
            Formiga temp = this.formignhas[i];
            for (int j = pos; j < this.cont; j++) {
                this.formignhas[j] = this.formignhas[j + 1];
            }
            this.cont--;
            return temp;
        }
    }

    @Override
    public UnorderedListADT<IFormiga> listaFormigas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
