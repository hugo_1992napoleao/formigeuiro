/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_furmigueiro;

import recursos.exceptions.ElementNotFoundException;
import recursos.exceptions.EmptyCollectionException;
import recursos.exceptions.FormigaCheiaException;
import recursos.interfaces.IComida;
import recursos.interfaces.IFormiga;

/**
 *
 * @author Lenovo
 */
public class Formiga implements IFormiga {

    private int id;
    private int capacidade_carga;
    private Comida[] comidinha;
    private int cont;

    public Formiga(int id, int capacidade_carga, Comida[] comidinha, int cont) {
        this.id = id;
        this.capacidade_carga = capacidade_carga;
        this.comidinha = comidinha;
        this.cont = cont;
    }

   
    @Override
    public int getCapacidadeCarga() {
        return this.capacidade_carga;
    }

    @Override
    public void setCapacidadeCarga(int i) {
        this.capacidade_carga = i;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int i) {
        this.id = i;
    }

    @Override
    public void addComida(IComida ic) throws FormigaCheiaException {
        if (this.cont >= this.capacidade_carga) {
            throw new FormigaCheiaException();
        } else {
            this.comidinha[this.cont] = (Comida) ic;
            this.cont++;
        }
    }

    private int pesquisa(int i) {
        int pos = -1, j = 0;

        while (j < this.cont || pos != -1) {
            if (i == this.comidinha[j].getId()) {
                pos = i;
            }
            j++;
        }

        return pos;
    }

    @Override
    public IComida removeComida(int i) throws EmptyCollectionException, ElementNotFoundException {
        if (this.cont == 0) {
            throw new EmptyCollectionException("Vazio");
        }
        int pos = pesquisa(i);
        if (pos == -1) {
            throw new ElementNotFoundException("Elemento não encontrado");
        } else {
            Comida temp = this.comidinha[i];
            for (int j = pos; j < this.cont; j++) {
                this.comidinha[j] = this.comidinha[j + 1];
            }
            this.cont--;
            return temp;
        }
    }

    @Override
    public IComida removeComida() throws EmptyCollectionException {
        if (this.cont == 0) {
            throw new EmptyCollectionException("Vazio");
        } else {
            Comida temp = this.comidinha[0];
            for (int j = 0; j < this.cont; j++) {
                this.comidinha[j] = this.comidinha[j + 1];
            }
            this.cont--;
            return temp;
        }
    }

    @Override
    public int getCarga() {
        return this.cont;
    }

}
