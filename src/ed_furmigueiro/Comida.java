/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_furmigueiro;

import recursos.interfaces.IComida;

/**
 *
 * @author Lenovo
 */
public class Comida implements IComida {

    private int id;
    private int tamanho;

    public Comida(int id, int tamanho) {
        this.id = id;
        this.tamanho = tamanho;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int i) {
        this.id = i;
    }

    @Override
    public int getTamanho() {
        return this.tamanho;
    }

    @Override
    public void setTamanho(int i) {
        this.tamanho = i;
    }

}
