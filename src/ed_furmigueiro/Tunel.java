/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_furmigueiro;

import recursos.interfaces.ITunel;

/**
 *
 * @author Lenovo
 */
public class Tunel implements ITunel {

    private int id;
    private int raio;
    private int distancia;

    public Tunel(int id, int raio, int distancia) {
        this.id = id;
        this.raio = raio;
        this.distancia = distancia;
    }

    
    @Override
    public int getDistance() {
        return this.distancia;
    }

    @Override
    public void setDistance(int i) {
        this.distancia = i;
    }

    @Override
    public int getRadious() {
        return this.raio;
    }

    @Override
    public void setRadious(int i) {
        this.raio = i;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int i) {
        this.id = i;
    }

}
